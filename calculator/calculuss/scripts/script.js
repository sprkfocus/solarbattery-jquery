 /*******************************************************
 * File: script.js
 * Author: Ivan Senin (senin-i-i@ya.ru)
 *******************************************************/

$(function()  //loading page elements 
{
	$(this).on("click", "button.delBtn", function()
	{
		$(this).closest("tr").hide(
			"highlight"
			, {}
			, 100
			, function()
			{
				$(this).remove();
				compute();
			});
	});

	$("#calculus").load(
		"calculuss/calc.html"
		, function()
		{
			$("#electronics").accordion(
			{
				autoHeight: false
				, collapsible: true
				, animated: 'slide'
				, heightStyle: "content"
			});

			$("div.Item").addClass("appliances");

			$(".appliances").draggable(
			{
				appendTo: "body"
				, helper: "clone"
				, start: function() 
				{
					$(this).hide();
					$("#dropSide").addClass("hovered");
				}
				, stop: function() 
				{
					$(this).show();
					$("#dropSide").removeClass("hovered");
				}
			});

			$("#userHouse").droppable(
			{
				accept: ".appliances"
				, drop: function(event, ui)
				{
					parameters = ($(ui.draggable).hasClass("solidBlock"))?
							grabFromCustom($(ui.draggable)) : grabFromImgItem($(ui.draggable));

					if (parameters.power <= 0)
					{
						alert("Установите значение мощности (" + parameters.name + ")");
						return;
					}
					makeNewLine(parameters);

					compute();
				} // drop
			});

			// power slider at the electronics box
			$("#electronics .slider").each(function() 
			{
				$(this).slider(
				{
					animate:'fast'
					, slide: function(event, ui) 
					{
						setPowerTooltipContent(ui.value);
					}
					, orientation: "vertical"
					, range: "min"
					, min: $(this).data("pmin")
					, max: $(this).data("pmax")
					, step: 5
					, value: $(this).data("pdefault")})
				.css({display: "inline-block"})
				.tooltip(
				{
					track: true
					, items: "div"
					, content: function()
					{
						return ("Мощность: " + $(this).slider( "option", "value" ) + " Вт");
					}
					, position: 
					{
						my: "left+15 center"
						, at: "right center"
					}
				});
			}) // each slider in #electronics

			setupEvents();

		}); // load

}); // page ready


function makeNewLine(parameters)
{
	parameters = $.extend({defTime: 7, isPowerPerYear: false}, parameters);
	name = (parameters.name.length > 0)? parameters.name.substring(0, 30) : "Прибор на " + parameters.power + " Вт";
	
	name = name.replace(/[\<\%\/\{\\"\\'\?\.\\\^\$\|]/g, "-");

	newRow = '<tr'
			+ ((parameters.custom)? ' class="customRow"' : '')
			+'>'
			+ '<td><button class = "delBtn">​X</button></td>'
			+ '<td>' +name + '</td>'
			+ '<td>' + '<input class="spinner">' + '</td>'
			+ '<td>' + '<div class="slider"></div><p></p>' + '</td>'
			+ '</tr>';

	$("#applianceTable tbody").append(newRow);

	setupTableSpinner();
	setupTableSlider(parameters);
	newRow = $("tr:last", "#applianceTable");
	setTrData(newRow, parameters);
	setupTableTooltip(newRow, parameters);
}

function compute()
{
	var dailySpending = 0;
	var mostPowerful = 0; // kW

	$("tr:has(button)", "#userHouse").each(function()
	{
		device = loadFromTableLine($(this));
		dailySpending += device.kW_h;
		if (((device.isResistive)? 3 * device.power : device.power) > mostPowerful) 
		{
			mostPowerful = device.count * ((device.isResistive)? 3 * device.power : device.power); // onstart engine power utilization
		}
	});

	monthSpending = 30 * dailySpending;

	accCapacity = monthSpending * 2 * 5;  // magic constants - ask CEO Kuzya. 

	var params = {};
	if (monthSpending >= 0)
	{
		params =
		{
			dailySpending: dailySpending.toFixed(2)
			, monthSpending: monthSpending.toFixed(2)
			, accCapacity: accCapacity.toFixed(2)
			, controller: (accCapacity * 0.1).toFixed(2)
			, inverter: (Math.ceil(mostPowerful * 10) / 10).toFixed(2)
		}
	}
	else
	{
		showConfiguration(null);
		return;
	}


	printOut(params);
	chooseGoods(params);
}

function chooseGoods(params)
{
	config12V = chooseGoodsWith(params, 12);
	config24V = chooseGoodsWith(params, 24);
	is12VMissed = hasNulls(config12V);
	is24VMissed = hasNulls(config24V);

	if (is12VMissed && !is24VMissed)
	{
		showConfiguration(config24V);
		return;
	}
	showConfiguration((config12V.totalPrice < config24V.totalPrice)? config12V : config24V);
}


function chooseGoodsWith(params, defaultVoltage)
{
	var config = {};
	config = $.extend(findInverter(params.inverter, defaultVoltage), config);
	config = $.extend(findAccumulator(params.accCapacity, config.inverter.voltage), config);
	config = $.extend(findController(params.controller, config.inverter.voltage), config);
	config = $.extend(findSolarBattery(params.controller, config.inverter.voltage), config);
	config = $.extend(calcTotalPrice(config), config);
	
	return config;
}

function findSolarBattery(controller, voltage)
{
	catalog = getCatalog("calculuss/catalog/battaries.dat");

	var bestBattery = null;
	var bestCount = Number.MAX_VALUE;
	var bestPenalty = Number.MAX_VALUE;

	for(key in catalog)
	{
		curBattery = catalog[key];
		amperage = Math.ceil(curBattery.power / curBattery.voltage * 5) / 5;
		bCount = Math.floor(controller / amperage);

		if (bCount == 0 || curBattery.voltage != voltage)
		{
			continue;
		}

		typePenalty = (curBattery.s_name.indexOf("poly") >= 0) ? 0 : 1;
		pricePenalty = 1.3 * Math.log(curBattery.price * bCount);
		countPenalty = Math.abs(bCount - 1.5);
		amperPenalty = Math.abs(amperage * bCount - controller) * 1.5;
		penalty = countPenalty + amperPenalty + typePenalty + pricePenalty;
		if (penalty <= bestPenalty)
		{
			bestPenalty = penalty;
			bestBattery = curBattery;
			bestCount = bCount;
			
		}
		//console.log("better battery:  " + bestCount + " [шт] of " + bestBattery.s_name + "\tTotal.penalty: " + penalty);
		//console.log("amper used: " + amperage * bCount + "/" + controller + "\tTotal bat.price: " + bCount * curBattery.price);
		//console.log(bCount + " [шт], amperage per one bat: " + amperage + " battery:\t\t" + curBattery.s_name);
	};

	//console.log("best battery:  " + bestCount + " [шт] of " + bestBattery.s_name);
	if (bestBattery != null)
	{
		bestBattery = $.extend({count : bestCount}, bestBattery);
	}

	config = {battery : bestBattery};
	return config;
}

function findController(controllerAmper, voltage)
{
	catalog = getCatalog("calculuss/catalog/controllers.dat");
	var bestController = null;
	var bestPenalty = Number.MAX_VALUE;

	for(key in catalog)
	{
		curController = catalog[key];
		if (curController.amperage < controllerAmper || $.inArray(voltage, curController.voltage) < 0)
		{
			continue;
		}

		extraAmperPenalty = Math.abs(curController.amperage - curController.amperage);
		pricePenalty = Math.log(curController.price);
		totalPenalty = extraAmperPenalty + pricePenalty;
		if (totalPenalty < bestPenalty)
		{
			bestPenalty = totalPenalty;
			bestController = curController;
		}
	};

	//console.log("best controller:  " + ((bestController != null)? bestController.s_name : "null"));

	config = {controller : bestController};
	return config;
}

function findInverter(inverterkW, defaultVoltage)
{
	catalog = getCatalog("calculuss/catalog/invertory.dat");
	var bestInverter = null;
	var minPenalty = Number.MAX_VALUE;

	for(key in catalog)
	{
		curInverter = catalog[key];
		if (curInverter.power < inverterkW || curInverter.voltage != defaultVoltage)
		{
			continue;
		}

		var penalty = Math.abs(curInverter.power - inverterkW);
		penalty += Math.log(curInverter.price);

		if (penalty < minPenalty)
		{
			minPenalty = penalty;
			bestInverter = curInverter;
		}
	};

	//console.log("best inverter: " + ((bestInverter != null)? bestInverter.s_name : "null"));

	config = {inverter : bestInverter};
	return config;
}

function findAccumulator(capacityAmpH, voltage)
{
	catalog = getCatalog("calculuss/catalog/acb.dat");

	var bestAccumulator = null;
	var bestCount = Number.MAX_VALUE;
	var bestPenalty = Number.MAX_VALUE;

	for(key in catalog)
	{
		curAccumulator = catalog[key];
		accCount = Math.ceil(capacityAmpH / curAccumulator.capacity);
		if (accCount == 0)
		{
			continue;
		}

		countPenalty = 1.5 * Math.abs(accCount - 1.5);
		capacPenalty = Math.abs(accCount * curAccumulator.capacity - capacityAmpH);
		pricePenalty = 1.5 * Math.log(curAccumulator.price * accCount);
		penalty = countPenalty + capacPenalty + pricePenalty;
		if (penalty <= bestPenalty)
		{
			bestPenalty = penalty;
			bestAccumulator = curAccumulator;
			bestCount = accCount;
		}
		
	};

	//console.log("best accumulator:  " + bestCount + " [шт] of " + bestAccumulator.s_name + " with " + bestAccumulator.capacity);
	bestAccumulator = $.extend({count : bestCount}, bestAccumulator);

	config = {accumulator : bestAccumulator};
	return config;
}




















//   ----------------         some aux functions below -------------------------//

function loadFromTableLine(trElem)
{
	monthCount = 12;
	daysPerMonth = 30;
	fridgeWorkingTime = 6;

	var isResistive = false;
	hasYearPower = $(trElem).data("isPerYear");
	
	power = $(trElem).data("power");
	count = $(".spinner", trElem).spinner("value");
	var constPower = 0;
	var kW_h = 0;
	var timeLenght = 1;

	if (hasYearPower) // value == kW_h * year
	{
		kW_h = (power / monthCount / daysPerMonth) * count; // kW_h * year -> kW_h * day
		constPower = power / monthCount / daysPerMonth / fridgeWorkingTime; // kw_h * year -> kW
	}
	else
	{
		timeLenght = sliderToHours($(".slider", trElem).slider( "option", "value"));
		kW_h = power * count * timeLenght / 1000; // W -> kW * h
		constPower = power / 1000;
	}


	info = 
	{
		power: constPower
		, count: count
		, isResistive: isResistive
		, kW_h: kW_h
		, isCustom: $(trElem).hasClass("customRow")
		, isResistive: $(trElem).data("isResistive")
	}
	return info;
}

function grabFromImgItem(item)
{
	var image = item.children("img").first();
	params = 
	{
		name: image.data("name")
		, defTime: image.data("default")
		, power: item.children(".slider").first().slider( "option", "value" )
		, custom: false
		, isResistive : isResistiveDevice(image.data("name"))
	}
	return params;
}

function grabFromCustom(item)
{
	var isFridgeStyle = item.has("img").length > 0;

	params = 
	{
		name: (isFridgeStyle)? $("img", item).data("name") : $("input", item).first().val()
		, defTime: (isFridgeStyle)? 24 : 5
		, power: (isFridgeStyle)?  $("select", item).val() : $(".slider", item).first().slider( "option", "value" )
		, custom: true
		, isPowerPerYear: isFridgeStyle
		, isResistive: (isFridgeStyle)? true : $("input[type = checkbox]", item).is("checked")
	}
	return params;
}

function setupTableSpinner()
{
	$(".spinner:last")
	.spinner(
	{
		min: 1
		, max: 999
		, stop: compute
	})
	.css({width: "40px"})
	.spinner("value", 1);
}

function setupTableSlider(parameters)
{
	var newSlider = $(".slider:last", "#applianceTable");
	if (parameters.isPowerPerYear)
	{
		newSlider.removeClass("slider").html("<b>auto</b>");
		return;
	}

	newSlider.slider(
	{
		animate:'fast'
		, slide: sliderChangedFreq
		, stop: compute
		, min: 0
		, max: 24 + 3
		, value: parameters.defTime + 3
	})
	.css(
	{
		width: "40%"
		, float: "left"
	})
	.next().html("<nobr>" + parameters.defTime + " час/сут</nobr>");
}

function setupTableTooltip(trElem, parameters)
{
	$(trElem)
	.tooltip(
	{
		track: true
		, items: "div"
		, content: "Мощность: " + parameters.power + " Вт"
		, position: 
		{
			my : "left+15 bottom-15"
			, at : "right top"
		}
	})
	.mouseover(function()
	{
		setPowerTooltipContent($(this).data("power"));
	});
}

function setupEvents()
{
	customCheckbox = $("input[type=checkbox]", "div.customItem.Item").first();
	customInput = $("input[type=text]", "div.customItem.Item").first();

	$("#applianceTable").mouseleave(closeTooltips);

	customCheckbox.change(function()
	{
		$(this).data("manual", true);
	});

	customInput.bind("input", function()
	{
		if (customCheckbox.data("manual"))
		{
			return;
		}
		if (isResistiveDevice(customInput.val()))
		{
			customCheckbox.prop("checked", true);
		}
		else
		{
			customCheckbox.prop("checked", false);
		}
	});
}

function sliderChangedFreq(event, ui) 
{
	$(this).next().html("<nobr>" + sliderToHours(ui.value) + " час/сут</nobr>");
}

function setPowerTooltipContent(content)
{
	$(".ui-tooltip-content").html("Мощность: " + content + " Вт");
}

function closeTooltips()
{
	$("div[id^='ui-tooltip-'").remove();
}

function sliderToHours(value)
{
	offset = 4;
	if (value <= offset)
	{
		return value / 4;
	}
	else
	{
		return value - (offset - 1);
	}
}

function setTrData(trElem, params)
{
	trElem.data("power", params.power);
	trElem.data("isPerYear", params.isPowerPerYear);
	trElem.data("isResistive", params.isResistive);
}

function getCatalog(url)
{
	var result = {};
	$.ajax(
	{
		type: "GET"
		, dataType: "json"
		, url: url
		, timeout: 2500
		, async: false
		, error: function(jqXHR, textstatus)
		{
			console.log("error: " + textstatus);
		}
		, success: function(response)
		{
			result = response;
		}
	});
	return result;
}

function hasNulls(config)
{
	return config.battery == null || config.controller == null || config.inverter == null || config.accumulator == null; 
}

function calcTotalPrice(config)
{
	var total = 0;
	total += config.battery.price * config.battery.count;
	total += (config.controller == null)? 0 : config.controller.price;
	total += (config.inverter == null)? 0 : config.inverter.price;
	total += config.accumulator.price * config.accumulator.count;

	result = {totalPrice : total};
	return result;
}

function appendConfiguretionRow(name, count, price)
{
	var line = "<tr class = 'item'><td>" + name + "</td><td>" + count + "</td><td>" + price + "</td></tr>";
	$(line).appendTo($("tbody", "div#configuration"));
}

function printOut(params)
{
	var results = "Дневное энергопотребление = " + params.dailySpending + " [кВт*ч]<br>"
			+ "Месячное потребление = " + params.monthSpending + " [кВт*ч]<br>"
			+ "Суммарная ёмкость АКБ = " + params.accCapacity + " [А*ч]<br>"
			+ "Мощность инвертора = " + params.inverter + " [кВт]<br>"
			+ "Контроллер = " + params.controller + " [A]<br>"
	$("div#calcProcess").html(results);
}

function showConfiguration(config)
{
	var lines = "";
	if (config == null)
	{
		lines = "Не удалось подобрать конфигурацию<br>";
		$("div#configuration").html(lines);
		return;
	}
	clearTableItems();
	showBattery(config.battery);
	showController(config.controller);
	showInverter(config.inverter);
	showAccumulator(config.accumulator);
	showTotalPrice(config.totalPrice);
}

function clearTableItems()
{
	$("tr.item", "div#configuration tbody").remove();
}

function showBattery(battery)
{
	if (battery != null)
	{
		appendConfiguretionRow("Батарея: " + battery.s_name, battery.count, battery.price);
	}
}

function showController(controller)
{
	if (controller != null)
	{
		appendConfiguretionRow("Контроллер: " + controller.s_name , 1, controller.price);
	}
}

function showInverter(inverter)
{
	if (inverter != null)
	{
		appendConfiguretionRow("Инвертор: " +inverter.s_name , 1, inverter.price);
	}
}

function showAccumulator(accumulator)
{
	if (accumulator != null)
	{
		appendConfiguretionRow("АКБ: " + accumulator.s_name , accumulator.count, accumulator.price);
	}
}

function showTotalPrice(price)
{
	var line = "";
	if (isNaN(price))
	{
		line = "Цена не определена<br>";
	}
	else
	{
		line = "Стоимость решения: " + price + " руб.</br>";
	}
	$("p#totalPrice", "div#configuration").html(line);
}


function isResistiveDevice(name)
{
	words = name.toLowerCase().split(" ");
	resistives = ["холодильник", "двигатель", "мотор", "насос", "резистивн"];
	for (index in words)
	{
		if (resistives.indexOf(words[index]) >= 0)
		{
			return true;
		}
	}
	return false;
}
